import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import img1 from '../img1.jpeg';


function Affiche() {
  return (
      <>
          <div className="col-xs-6 col-sm-6 col-md-4 col-lg-3">
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={img1} />
              <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                  Some quick example text to build on the card title and make up the bulk of
                  the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
              </Card.Body>
            </Card>
          </div>
          <div className="col-xs-6 col-sm-6 col-md-4 col-lg-3">
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={img1} />
              <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                  Some quick example text to build on the card title and make up the bulk of
                  the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
              </Card.Body>
            </Card>
          </div>
          <div className="col-xs-6 col-sm-6 col-md-4 col-lg-3">
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={img1} />
              <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                  Some quick example text to build on the card title and make up the bulk of
                  the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
              </Card.Body>
            </Card>
          </div>
          <div className="col-xs-6 col-sm-6 col-md-4 col-lg-3">
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={img1} />
              <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                  Some quick example text to build on the card title and make up the bulk of
                  the card's content.
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
              </Card.Body>
            </Card>
          </div>
       </>
  );
}

export default Affiche;
