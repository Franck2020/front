import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom"
import Affiches from './Affiches.js';
import Accommodations from './Accommodations.js';
import Restaurants from './Restaurants.js';
import Bars from './Bars.js';
import Transports from './Transports.js';

function Home() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <Router>
      <div className="Application">
        <header>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand" href="/EPic">EpicRoadTrip</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <Link className="nav-item nav-link active" to="/">Events</Link>
                <Link className="nav-item nav-link" to="/Accommodations">Accommodations</Link>
                <Link className="nav-item nav-link" to="/Transports">Transports</Link>
                <Link className="nav-item nav-link" to="/Restaurants">Restaurants</Link>
                <Link className="nav-item nav-link" to="/Bars">Bars</Link>
                <Link className="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</Link>
              </div>
            </div>
          </nav>
        </header>
        <section className="container">
          <div style={{ top: '1em' }} className="row align-items-center alert alert-primary header_c">
              <h3 className='col-lg-6'>Affichage</h3>
              <div className='col-lg-6'><svg variant="primary" onClick={handleShow} className="btn" style={{ cursor: 'pointer' }} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z"/>
              </svg></div>
          </div>
          <div style={{ marginTop: '2em' }} className='row  corps_element'>
              <Switch>
                    <Route exact path='/' component={Affiches} />
                    <Route exact path='/Transports' component={Transports} />
                    <Route exact path='/Restaurants' component={Restaurants} />
                    <Route path='/Accommodations' component={Accommodations} />
                    <Route path='/Bars' component={Bars} />
              </Switch>
          </div>
        </section>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Filter</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form action ="" data-url="controleur/enregistrement.php" method="post" id="form_enregistrement">
            <div className="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
              <label>
                Date: 
              <input type="date" className="form-control" name="datess" required />
              </label>
            </div>
            <div className="input-group col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
              <label>
                Max price: 
              <input type="number" className="form-control" name="price" placeholder='Ex: 120€ ' required />
              </label>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
              <label>Location: </label>
              <select className="form-control" name="bi_num">
                <option value='Adequat'>Choose</option>
                <option value='Adequat'>Paris</option>
                <option value='Planete'>Lyon</option>
              </select>
            </div>
            
          </form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="success" onClick={handleClose}>
              Go
            </Button>
          </Modal.Footer>
        </Modal>
        
      </div>
    </Router>
  );
}

export default Home;
